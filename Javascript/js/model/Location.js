window.Explore = window.Explore || {};
//splitting the response location
var getGeolocation = function(loc) {
	var split = loc.split(',');
	return {
		lat: parseFloat(split[0]),
		lng: parseFloat(split[1])
	};
}

window.Explore.Locations = new (function() {
	this.placeList = [];
	this.catAndPlaceMap = window.Explore.categoryAndPlaceMap;
	var self = this;

	var parseResponse = function(response) {
		var locations = [];
		var locationArray = response.location;
		for (var loc in locationArray) {
			locationArray[loc].point = getGeolocation(locationArray[loc].location);
			locationArray[loc].marker=window.Explore.placeMarker.addMarker(locationArray[loc]);
			var category  =locationArray[loc].category;
			if (!self.catAndPlaceMap.contains(category)) {
			    self.catAndPlaceMap.put(category, []);
			}
			self.catAndPlaceMap.catMarker[category].push(locationArray[loc].marker);
			locations.push(locationArray[loc]);
		}
		window.Explore.filterPanel.populate();
		return locations;
	};
	var enableInputs = function () {
	    window.Explore.filterPanel.enableFilterSearchBox();
	    window.Explore.customGoogleMap.enableSearchBox();
	}

	var disbleInputs = function () {
	    window.Explore.filterPanel.disableFilterSearchBox();
	    window.Explore.customGoogleMap.disableSearchBox();
	}

	this.getLocations = function(options) {
	    this.placeList = [],
		this.categoryAndPlaceMap = {},
        disbleInputs();
        window.Explore.filterPanel.showCount(0);
		fetchLocations(options);
	};

	var fetchLocations = function(options) {
		//options.q = escape(options.q);
		var onPass = function(response) {
			var locs = parseResponse(response);
			self.placeList = self.placeList.concat(locs);
		}
		$.ajax({
			url: "http://explore.appacitive.com/locations?lat="+options.pt.lat+"&lng="+options.pt.lng+"&rad="+options.rad+"&pnum="+options.pnum+"&psize="+options.psize+"&q="+options.q,
			context: document.body
		}).done(function(response) {
			if (response.pi.totalrecords > options.pnum * options.psize) {
				options.pnum++; 
				fetchLocations(options);
			}
			else
			{
			    window.Explore.filterPanel.hideProgressBar();
			    enableInputs();
			}
			onPass(response);
		});
	};
})();