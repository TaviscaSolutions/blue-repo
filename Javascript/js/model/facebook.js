﻿//defining namespace
window.Explore = window.Explore || {}

//creating self-excuting function 
window.Explore.Facebook = new (function () {
    this.getAppId = function () {      //this function will return AppId
        return "854974184527484";
    }
    this.getVersion = function () {     //this function will return version number
        return "v2.0";
    }
    var self = this;
    this.initialize = function () {

        //Load the SDK Asynchronously
        (function (d, s, id) {
            //get first script element , use for finding parent
            var js, fjs = d.getElementsByTagName(s)[0];
            //if sdk is already installed ,then we are done
            if (d.getElementById(id)) return;
            //craete a new script element and set its id
            js = d.createElement(s); js.id = id; js.async = true;
            //set source of new element to the source of FB JS SDK
            js.src = "http://connect.facebook.net/en_US/sdk.js#xfbml=1&appId="
                + self.getAppId() + "&version=" + self.getVersion();
            //this will insert FB JS SDK into DOM
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    }

    //window.fbAsyncInit = function () {
    //    FB.Event.subscribe('auth.logout', function (response) {
    //        window.location.reload();
    //    });
    //}

})()

window.Explore.Facebook.initialize();   //call to initialize function

