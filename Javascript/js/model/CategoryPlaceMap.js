window.Explore = window.Explore || {};

window.Explore.categoryAndPlaceMap = new (function() {
		this.categories = [];
		this.catMarker = {};

		this.init = function(){
			this.categories = [];
			this.catMarker = {};
		}
		this.put = function(key,value) {
		    if (this.catMarker[key] == null) {
				this.categories.push(key);
			}
			this.catMarker[key] = value;
		};

		this.get = function(key) {
		    return this.catMarker[key];
		};

		this.contains = function(key) {
			return $.inArray(key,this.categories)>-1;
		};

		this.get = function(key) {
			return this.catMarker[key];
		};
	})();