window.Explore = window.Explore || {};

var defaultBounds = null;

window.Explore.customGoogleMap = new (function () {
    this.isDefaultMarker = false;

    this.options = {
        pt: { lat: 18.5507626, lng: 73.95021849999999 },
        pnum: 1,
        psize: 200,
        q: '',
        rad: 5
    };

    this.mapOption = {
        zoom: 14,
        center: new google.maps.LatLng(this.options.pt.lat, this.options.pt.lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.BOTTOM_CENTER
        },
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        scaleControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        }
    };

    self = this;

    this.googleMap = null;

    this.defaultMarker = new google.maps.Marker({
        position: null,
        map: this.googleMap,
        icon: null,
        clickable: true,
        title: null,
        animation: google.maps.Animation.BLIND,
        infoWindowOfMarker: null,
    });

    this.disableSearchBox = function () {
        $('#txtSearch').attr('disabled', 'aabled');
    }

    this.enableSearchBox = function () {
        $('#txtSearch').removeAttr('disabled');
    }
    /*
   Summary : Sets all the parameter required to fetch data of particulat location
   --Parameters :  lat : Latitude of location
                   lng : Longitude of location
                   pnum : Initial page number
                   psize : page size (No .of records to be fetched at a time)
                   q : querystring to limit search to specific area
                   rad : radius for search
   */
    this.bindAjaxCallOptions = function (op) {
        this.options = $.extend({ pt: { lat: 18.5507626, lng: 73.95021849999999 }, pnum: 1, psize: 200, q: '', rad: 5 }, op);
    }

    /*
   Summary : Sets Map Opptions
   --Parameters : zoom : zoom level of map
                  center : center of map 
                  mapTypeId : The initial Map mapTypeId. Defaults to ROADMAP.
   */
    this.setMapOptions = function () {
        mapOption.center = new google.maps.LatLng(options.pt.lat, options.pt.lng);
    }

    this.initialiseMap = function () {
        //Bind map options...
        this.bindAjaxCallOptions(this.options);

        //Initialise Map
        mapContainer = document.getElementById('mapView');
        self.googleMap = new google.maps.Map(mapContainer, self.mapOption);
        //Render Map...
        this.googleMap.panTo(new google.maps.LatLng(self.options.pt.lat, self.options.pt.lng), self.options);
        var input = document.getElementById('txtSearch');

        //Initialise Autocomplete options...
        var autocompleteOptions = {
            bounds: defaultBounds,
            types: ['geocode']
        };

        autocomplete = new google.maps.places.Autocomplete(input, autocompleteOptions);
        var isPlaceChanged = false;
        var addDefaultMarker = function () {
            google.maps.event.addListener(self.defaultMarker, 'click', (function (defaultMarker) {
                return function () {
                    defaultMarker.infoWindowOfMarker.setContent('<h4>' + input.value + '</h4>');
                    if (window.Explore.infowindowObj.isInfoWindowOpen(defaultMarker.infoWindowOfMarker)) {
                        defaultMarker.infoWindowOfMarker.close();
                    } else {
                        defaultMarker.infoWindowOfMarker.open(defaultMarker.map, defaultMarker);
                    }
                }
            })(self.defaultMarker));
        }

        var defaultMarkerSetOption = function (lat, lng) {
            window.Explore.placeMarker.removeMarker();
            if (self.isDefaultMarker === true) {
                self.defaultMarker.setMap(null);
                self.isDefaultMarker = false;
            }

            image = {
                url: 'Images/Markers/marker_userlocation.png'
            }
            var markerLatLng = new google.maps.LatLng(lat, lng);
            self.defaultMarker.setOptions({
                position: markerLatLng,
                map: window.Explore.customGoogleMap.googleMap,
                icon: image,
                clickable: true,
                title: input.value,
                animation: google.maps.Animation.BLIND,
                infoWindowOfMarker: window.Explore.infowindowObj.infowindow,
            });
            self.isDefaultMarker = true;
            addDefaultMarker();
        }

        var _initilizeGoogleMapAndGetLocations = function (op) {
            window.Explore.filterPanel.showProgressBar();
            self.googleMap.panTo(new google.maps.LatLng(self.options.pt.lat, self.options.pt.lng), self.options);
            window.Explore.Locations.catAndPlaceMap.init();
            window.Explore.Locations.getLocations(op);
        }
        
                  $('#txtSearch').on("click", function () {
          $(this).select();
          });

        $('#txtSearch').on('keyup', function (e) {
            if (e.keyCode === 13) {
                geocoder = new google.maps.Geocoder();
                var address = document.getElementById('txtSearch').value;
                geocoder.geocode({ 'address': address }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var op = { pt: { lat: 0, lng: 0 }, pnum: 0, psize: 0, q: '', rad: 0 };
                        op.pt.lat = results[0].geometry.location.lat();
                        op.pt.lng = results[0].geometry.location.lng();
                        op.pnum = 1;
                        op.psize = 200;
                        op.rad = 5;
                        op.pt.q = document.getElementById('txtSpecificSearch').value;

                        self.bindAjaxCallOptions(op);
                        defaultMarkerSetOption(op.pt.lat, op.pt.lng);
                        _initilizeGoogleMapAndGetLocations(op);
                    }
                });
            }
        });
    }

    /*
   Summary : Resets to current location.
   --Parameters : self : reference of current context.
   */
    this.resetLocation = function () {
        //Sets lat lng of default location
        /*jQuery.ajax({
            url: '//freegeoip.net/json/',
            type: 'POST',
            dataType: 'jsonp',
            success: function (location) {
               var lat = location.longitude;
              var lng = location.latitude;
            }
         });*/
        if (navigator.geolocation) {
            var latCurrent = position.coords.latitude;
            var lngCurrent = position.coords.longitude;
        }

        var op = { pt: { lat: latCurrent, lng: lngCurrent }, pnum: 1, psize: 200, q: '', rad: 5 };
        this.bindAjaxCallOptions(op);
        //Set map options
        this.googleMap.panTo(new google.maps.LatLng(self.options.pt.lat, self.options.pt.lng), self.options);
        //Get data for current location
        window.Explore.Locations.getLocations(this.options);
        window.Explore.Locations.catAndPlaceMap.init();
    }
})();

$(function () {
    window.Explore.customGoogleMap.initialiseMap();
    document.getElementById('lnkSetCurrent').addEventListener("click", function () {
        /*
        Ideally this should be done using navigator.geolocation.getcurrentposition,
        but this is not working here so we are hardcoding it for now.
        */
        window.Explore.customGoogleMap.resetLocation();
    });
    window.Explore.Locations.getLocations(window.Explore.customGoogleMap.options);

});