window.Explore = window.Explore || {};

function marker(){

    this.addMarker=function (locationArray) {            
       
        var markerData = locationArray;
    
        var urlIt = "Images/Markers/marker_" + markerData.category + ".png"; //get image as per category
        var image = {
            url: urlIt,          
        };

        var markerLatLng = new google.maps.LatLng(markerData.point.lat, markerData.point.lng);
        var marker = new google.maps.Marker({
            position: markerLatLng,
            map: window.Explore.customGoogleMap.googleMap,
            icon: image,
            clickable: true,
            title: markerData.name,
            //animation: google.maps.Animation.DROP,
            infoWindowOfMarker: window.Explore.infowindowObj.infowindow,
        });         
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function (marker,markerData) {
            
            return function () {
                marker.infoWindowOfMarker.close();
                marker.infoWindowOfMarker.setContent(window.Explore.infowindowObj.setContentOfInfoWindow(markerData));                                                        
                // marker.infoWindowOfMarker.open(marker.map, marker);
                window.Explore.infowindowObj.openInfoWindow(marker,markerData);               
            }            
        })(marker,markerData));

        return marker;
}
this.hideMarker = function (cat) {
    for (var pos in window.Explore.Locations.catAndPlaceMap.catMarker[cat]) {
        window.Explore.Locations.catAndPlaceMap.catMarker[cat][pos].setVisible(false);
        window.Explore.Locations.catAndPlaceMap.catMarker[cat][pos].infoWindowOfMarker.close();
    }
}

this.showMarker = function (cat) {
    for (var pos in window.Explore.Locations.catAndPlaceMap.catMarker[cat]) {
        window.Explore.Locations.catAndPlaceMap.catMarker[cat][pos].setVisible(true);
        window.Explore.Locations.catAndPlaceMap.catMarker[cat][pos].infoWindowOfMarker.close();
    }
}


this.removeMarker = function () {
    for (var i in window.Explore.Locations.placeList)
    {
        window.Explore.Locations.placeList[i].marker.setMap(null);
    }
}

}

window.Explore.placeMarker = new marker();

