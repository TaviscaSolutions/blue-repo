window.Explore = window.Explore || {};

window.Explore.filterPanel  = new (function () {
    
    var _functionToggleClass = function () {
        $.each($('#results span.category'), function (index, category) {
            var categoryId = $(this).attr('data-val');
            $($(category).parent()).click(function () {
                if ($(category).parent().hasClass('map-geocoder active')) {
                    window.Explore.placeMarker.hideMarker(categoryId);
                }
                else {
                    window.Explore.placeMarker.showMarker(categoryId);
                }
                $(category).parent().toggleClass('active');
            });
        });

    }

    this.categoryCount = new Array();
    this.populate = function () {
        var count;
        var totalCount = 0;
        this.categoriesArray = window.Explore.Locations.catAndPlaceMap.categories;
        for (var i = 0; i < this.categoriesArray.length; i++) {
            count = 0;
            count = window.Explore.Locations.catAndPlaceMap.catMarker[this.categoriesArray[i]].length;
            this.categoryCount[i] = 0;
            totalCount += count;
            if (this.categoryCount[i] === undefined)
                this.categoryCount[i] = 0;
            this.categoryCount[i] = count;
        }
        this.showCount(totalCount);
    }
    this.showCount = function (totalCount) {
        var source = $("#categoryListTemplate").html();
        var template = Handlebars.compile(source);
        var categoryCollection = {
            noOfRecords: 0,
            filters: [
            {
                name: 'Food',
                count: 0,
                value: 'food'
            },
            {
                name: 'Coffee',
                count: 0,
                value: 'coffee'
            },
            {
                name: 'Outdoors',
                count: 0,
                value: 'outdoors'
            },
            {
                name: 'Drinks',
                count: 0,
                value: 'drinks'
            },
            {
                name: 'Shops',
                count: 0,
                value: 'shops'
            },
            {
                name: 'Arts',
                count: 0,
                value: 'arts'
            }
            ]
        };

        for (var i = 0; i < categoryCollection.filters.length; i++) {
            if (window.Explore.Locations.catAndPlaceMap.get(categoryCollection.filters[i].value) != null) {
                categoryCollection.filters[i].count = window.Explore.Locations.catAndPlaceMap.get(categoryCollection.filters[i].value).length;
            }
        }
        categoryCollection.noOfRecords = totalCount;

        $("#results").html(template(categoryCollection));
        _functionToggleClass();
    }
    this.hideProgressBar = function () {
        $("#img-process").css("visibility", "hidden");
    }
    this.showProgressBar = function () {
        $("#img-process").css("visibility", "visible");
    }
    this.disableFilterSearchBox = function () {
        $('#txtSpecificSearch').attr('disabled', 'disabled');
    }

    this.enableFilterSearchBox = function () {

        $('#txtSpecificSearch').removeAttr('disabled');
    }

    this.bindEvents: function() {
        
        var _element = document.getElementById("txtSpecificSearch");
        _element.addEventListener("keypress", function (event) {
             if (event.keyCode === 13) { //enter key
                 window.Explore.filterPanel.showProgressBar();
                 var searchQuery = $('#txtSpecificSearch').val();
                 window.Explore.customGoogleMap.options.q = searchQuery;
                 window.Explore.customGoogleMap.options.pnum = 1;
                 window.Explore.Locations.catAndPlaceMap.init();
                 for (var index in window.Explore.Locations.placeList) {
                     window.Explore.Locations.placeList[index].marker.setMap(null);
                 }
                 window.Explore.Locations.getLocations(window.Explore.customGoogleMap.options);
             }
        });
    }

    var self = this;

})();

$(function(){
    window.Explore.filterPanel.bindEvents();    
})
