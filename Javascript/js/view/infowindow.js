﻿window.Explore = window.Explore || {};



//var markersArray = [];
/*var infowindow = new google.maps.InfoWindow({
    maxWidth: 200
});*/


function infowindow() {

    this.infowindow = new google.maps.InfoWindow({
        maxWidth: 300
    });

    this.setContentOfInfoWindow = function (markerData) {
        var source = $("#infoWindowTemplate").html();
        var template = Handlebars.compile(source);
        var photo;
        if (markerData.images.length === 0) {
            photo = 'Images/categories/' + markerData.category + '-icon.png';
        } else {
            photo = markerData.images[0];
        }
        
        var data = {
            id: markerData.__id, name: markerData.name, address:markerData.address, street: markerData.street, cityState: markerData.city + ',' + markerData.country, photo: photo
        };
        //$("#content").html(template(data));  
        var content = (template(data));
        return content;
    }

    this.openInfoWindow = function (marker,markerData) {
        var add;
            var placeholder = $('.overlay');
            placeholder.append(marker.infoWindowOfMarker.open(marker.map, marker));
            var borderShow = $('.overlay').parent().parent().parent().siblings();
            borderShow.addClass('custom-template');           
           // var m = $('.overlay').parent().parent().parent().children().first().children().children().first().children().parent().children();
            //m.addClass('corner');
  
    }
}

window.Explore.infowindowObj = new infowindow();